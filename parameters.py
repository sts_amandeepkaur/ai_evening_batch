def student(name,email="test@domain.com",rno=0):
    print("Name: {}, Email: {}, R.No: {}".format(name,email,rno))

#Positional Arguments
student("Aman","aman@gmail.com",1001)

#Keyword Arguments
student(rno=1002,name="Harry Potter",email="potter@yahoo.com")
student("Harry Potter",rno=1003,email="harry@yahoo.com")

#Default Argument
student("shinchan")
student("shinchan","nohara@yahoo.com")
student("nobita","nobita@nobi.com",1004)

'''
1. Positional arguments
2. Keyword Arguments
3. Default Arguments
4. Arbitary Arguments
'''
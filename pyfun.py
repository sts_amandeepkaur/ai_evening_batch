#Non Parameterized
def hello(): #definition
    print("I am a function in python") #body

hello() #calling

#Parameterized
def add(x,y):
    print(x+y)

add(10,20)

options = '''
    Press 1: To create Account
    Press 2: To Check Balance
    Press 3: To withdraw
    Press 4: To deposit
    Press 0: To exit
'''
print(options)
import random

customers = []
def create_account():
    name = input("Enter Your Name: ")
    bal = float(input("Enter Initial Amount: "))
    acc = random.randint(1000,9999)
    cust = {"name":name,"balance":bal,"Account":acc}
    customers.append(cust)
    print("Dear {} your acc. created successfully, balance:Rs.{}/- Acc No.:{}\n".format(name,bal,acc))

def check_bal():
    acc = int(input("Enter Account Number: "))
    found=False
    for i in customers:
        if i["Account"] == acc:
            found=True
            print("Welcome {} !!!".format(i["name"]))
            print("Your Balance: Rs.{}/-".format(i["balance"]))
    if found==False:
        print("Invalid Account Number")

def withdraw():
    acc = int(input("Enter Account Number: "))
    found=False
    for i in customers:
        if i["Account"] == acc:
            found=True
            print("Welcome {} !!!".format(i["name"]))
            amt = float(input("Enter Amount to withdraw: "))
            if i["balance"] < amt:
                print("Your Account has not sufficient balace, balance is:Rs.{}/-".format(i["balance"]))
            else:
                i["balance"] = i["balance"] - amt
                print("Your Acc withdraw with Rs.{}/- Current Balance:Rs.{}/-".format(amt,i["balance"]))
    if found==False:
        print("Invalid Account Number")
        
while True:
    op = input("Enter choice: ")
    if op=="1":
        create_account()
    elif op=="2":
        check_bal()
    elif op=="3":
        withdraw()
    elif op=="0":
        break
    else:
        print("Invalid Choice")